const express = require('express');
const Booking = require('../models/booking');


function list(req, res, next) {
    Booking.find().populate('_member _copy').then(objs => res.status(200).json({
        message: res.__('bookings.list.ok'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('bookings.list.bad'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Booking.findOne({'_id':id}).populate('_member _copy').then(obj => res.status(200).json({
        message: res.__('bookings.index.ok') + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('bookings.index.bad') + ` ${id}`,
        objs: err
    }));
}

function create(req, res, next) {
    const {
        copy,
        member,
        date
    } = req.body;

    let booking = new Booking({
        copy: copy,
        member: member,
        date: date
    });

    booking.save().then(obj => res.status(200).json({
        message: res.__('bookings.create.ok'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('bookings.create.bad'),
        objs: err
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    const booking = new Object();

    const {
        copy,
        member,
        date
    } = req.body;

    if(copy){
        booking._copy = copy;
    }

    if(member){
        booking._member = member;
    }

    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id}, booking).then(
        obj=>res.status(200).json({
            message: res.__('bookings.edit.ok') + ` ${id}`,
            obj: obj
        })
    ).catch(
        ex=>res.status(500).json({
            message: res.__('bookings.edit.bad') + ` ${id}`,
            obj: ex
        })
    );
}

function replace(req, res, next) {
    const id = req.params.id;
    const {
        copy,
        member,
        date
    } = req.body;

    let booking = new Object({
        _copy: copy,
        _member: member,
        _date: date
    });

    Booking.findOneAndReplace({'_id':id}, booking).then(obj => res.status(200).json({
        message: res.__("bookings.replace.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("bookings.replace.bad") + ` ${id}`,
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__("bookings.destroy.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("bookings.destroy.bad") + ` ${id}`,
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
