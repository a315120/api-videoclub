const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const User = require('../models/user');

//RESTFULL => GET, POST, PATCH, DELETE
//Modelo=(Una estructura de datos que representa una entidad del mundo real)
function list(req, res, next) {
  let page = req.params.page ? req.params.page: 1;
  User.paginate({}, {page:page, limit:3}).then(objs => res.status(200).json({
    message: res.__("users.list.ok"),
    obj:objs
  })).catch(ex => res.status(500).json({
    message: res.__("users.list.bad"),
    obj: ex
  }));
}

function index(req, res, next){
  const id = req.params.id;
   User.findOne({"_id":id}).then(obj => res.status(200).json({
     message: res.__("users.index.ok") + ` ${id}`,
     obj: obj
   })).catch(ex => res.status(500).json({
     message: res.__("users.list.bad"),
     obj: ex
   }));
}

const create = (req, res, next) => {

  let email = req.body.email;
  let name = req.body.name;
  let lastName = req.body.lastName;
  let password = req.body.password;

  async.parallel({
      salt: (callback) => {
          bcrypt.genSalt(10, callback);
      }
  }, (err, result) => {
      bcrypt.hash(password, result.salt, (err, hash) => {
          let user = new User({
              _email: email,
              _name: name,
              _lastName: lastName,
              _password: hash,
              _salt: result.salt
          });

          user.save().then(obj => res.status(200).json({
              message: res.__("users.create.ok"),
              obj: obj
          })).catch(ex => res.status(500).json({
              message: res.__("users.create.bad"),
              obj: ex
          }));
      });
  });
}

function replace(req, res, next){
  const id = req.params.id ? req.params.id: "" ;
  let email = req.body.email ? req.body.email: "";
  let name = req.body.name ? req.body.name: "";
  let lastName = req.body.lastName ? req.body.lastName: "";
  let password = req.body.password ? req.body.password: "";

  let user = new Object({
    _email: email,
    _name: name,
    _lastName: lastName,
    _password: password
  });

  User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
    message: res.__("users.replace.ok") + ` ${id}`,
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__("users.replace.bad"),
    obj: ex
  }));
}

function edit(req, res, next){
  const id = req.params.id;
  let name = req.body.name;
  let lastName = req.body.lastName;
  let email = req.body.email;
  let password = req.body.password;

  let user = new Object();

  if(name){
      user._name = name;
  }
  if(lastName){
      user._lastName = lastName;
  }
  if(email){
      user._email = email;
  }
  if(password){
      user._password = password;
  }

  User.findOneAndUpdate({"_id":id}, user).then(obj=>res.status(200).json({
      message: res.__('users.edit.ok'),
      obj:obj
  })).catch(ex=>res.status(500).json({
      message: res.__('users.edit.ok'),
      obj:ex
  }));
}

function destroy(req, res, next){
  const id = req.params.id;
  User.remove({"_id":id}).then(obj => res.status(200).json({
    message: res.__("users.destroy.ok"),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__("users.destroy.bad"),
    obj: ex
  }));
}

module.exports={
  list, index, create, replace, edit, destroy
}
