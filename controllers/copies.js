const express = require('express');
const Copy = require('../models/copy');

function list(req, res, next) {
    Copy.find().populate('_movie').then(objs => res.status(200).json({
        message: res.__("copies.list.ok"),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__("copies.list.bad"),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Copy.findOne({'_id':id}).populate('_movie').then(obj => res.status(200).json({
        message: res.__("copies.index.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("copies.list.bad"),
        objs: err
    }));
}

function create(req, res, next) {
    const {
        format,
        movie,
        number,
        status
    } = req.body;

    let copy = new Copy({
        format: format,
        movie: movie,
        number: number,
        status: status
    });

    copy.save().then(obj => res.status(200).json({
        message: res.__("copies.create.ok"),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("copies.create.bad"),
        objs: err
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    const copy = new Object();

    const {
        format,
        movie,
        number,
        status
    } = req.body;

    if(format){
        copy._format = format;
    }

    if(movie){
        copy._movie = movie;
    }

    if(number){
        copy._number = number;
    }

    if(status){
        copy._status = status;
    }


    Copy.findOneAndUpdate({"_id":id}, copy).then(
        obj=>res.status(200).json({
            message: res.__("copies.edit.ok"),
            obj: obj
        })
    ).catch(
        ex=>res.status(500).json({
            message: res.__("copies.edit.bad"),
            obj: ex
        })
    );
}

function replace(req, res, next) {
    const id = req.params.id;
    const {
        format,
        movie,
        number,
        status
    } = req.body;

    let copy = new Object({
        _format: format,
        _movie: movie,
        _number: number,
        _status: status
    });

    Copy.findOneAndReplace({'_id':id}, copy).then(obj => res.status(200).json({
        message: res.__("copies.replace.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("copies.replace.bad") + ` ${id}`,
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__("copies.destroy.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("copies.destroy.bad"),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
