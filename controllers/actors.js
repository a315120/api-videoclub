const express = require('express');
const Actor = require('../models/actor');

//RESTFULL => GET, POST, PATCH, DELETE
//Modelo=(Una estructura de datos que representa una entidad del mundo real)
function list(req, res, next) {
  Actor.find().then(objs => res.status(200).json({
    message: res.__('actores.list.ok'),
    obj:objs
  })).catch(ex => res.status(500).json({
    message: res.__('actores.list.bad'),
    obj: ex
  }));

}

function index(req, res, next){
  const id = req.params.id;
  Actor.findOne({"_id":id}).then(obj => res.status(200).json({
    message: res.__('actores.index.ok') + `${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: res.__('actores.index.bad') + `${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;

  let actor = new Actor({
    name:name,
    lastName:lastName
  });

  actor.save().then(obj => res.status(200).json({
    message: res.__('actores.create.ok'),
    obj: obj
  })).catch(ex => res.status(500).json({
    message: res.__('actores.create.bad'),
    obj: ex
  }));

}

function replace(req, res, next){
  const id = req.params.id;
  let name = req.body.name ? req.body.name: "";
  let lastName = req.body.lastName ? req.body.lastName: "";

  let actor = new Object({
    _name: name,
    _lastName: lastName
  });

  Actor.findOneAndUpdate({"_id":id}, actor).then(obj => res.status(200).json({
    message: res.__('actores.replace.ok'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('actores.replace.bad'),
    obj: ex
  }));

}

function edit(req, res, next){

  const id = req.params.id;
  let name = req.body.name;
  let lastName = req.body.lastName;

  let actor = new Object();

  if(name){
    actor._name = name;
  }

  if(lastName){
    actor._lastName = lastName;
  }

  Actor.findOneAndUpdate({"_id":id}, actor).then(obj => res.status(200).json({
    message: res.__('actores.edit.ok'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('actores.edit.bad'),
    obj: ex
  }));

}

function destroy(req, res, next){
  const id = req.params.id;
  Actor.remove({"_id":id}).then(obj => res.status(200).json({
    message: res.__('actores.destroy.ok'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('actores.destroy.bad'),
    obj: ex
  }));
}

module.exports={
  list, index, create, replace, edit, destroy
}
