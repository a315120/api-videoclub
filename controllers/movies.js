const express = require('express');
const Movie = require('../models/movie');

function list(req, res, next) {
    Movie.find().populate('_actors').then(objs => res.status(200).json({
        message: res.__("movies.list.ok"),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__("movies.list.bad"),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Movie.findOne({'_id':id}).populate('_actors').then(obj => res.status(200).json({
        message: res.__("movies.list.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("movies.list.bad"),
        objs: err
    }));
}

function create(req, res, next) {
    const {
        genre,
        title,
        directorName,
        directorLastName,
        actors
    } = req.body;

    const director = new Object({
        _name: directorName,
        _lastName: directorLastName
    });

    let movie = new Movie({
        genre: genre,
        title: title,
        director: director,
        actors: actors
    });

    movie.save().then(obj => res.status(200).json({
        message: res.__("movies.create.ok"),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("movies.create.bad"),
        objs: err
    }));
}

function edit(req, res, next){
    let id = req.params.id;

    let director = {
        name:req.body.directorName,
        lastName:req.body.directorLastName,
    }
    const actors = {
        name:req.body.actorName, 
        lastName:req.body.actorLastName
    }
    let genre = req.body.genre;
    let title = req.body.title;

    let movie = new Object();

    if(director){
        movie._director = director;
    }
    if(actors){
        movie._actors = actors;
    }
    if(genre){
        movie._genre = genre;
    }
    if(title){
        movie._title = title;
    }

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj=>res.status(200).json({
        message: res.__('movies.edit.ok'),
        obj:obj
    })).catch(ex=>res.status(500).json({
        message: res.__('movies.edit.bad'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;

    let director = {
        name:req.body.directorName ? req.body.directorName: "",
        lastName:req.body.directorLastName ? req.body.directorLastName:"",
    }
    let actors = {
        name:req.body.actorName ? req.body.actorName: "",
        lastName:req.body.actorLastName ? req.body.actorLastName:"",
    }
    let genre = req.body.genre ? req.body.genre: "";
    let title = req.body.title ? req.body.title: "";

    let movie = new Object({
        director:director,
        actors:actors,
        genre:genre,
        title:title,
    });

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
        message: res.__('movies.replace.ok'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('movies.replace.bad'),
        obj:ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__("movies.destroy.ok") + ` ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__("movies.destroy.bad"),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
