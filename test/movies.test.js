const supertest = require('supertest');
const app = require('../app');

var key="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'a315120@uach.mx', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('Probar get en ruta de movies', ()=>{
  it('deberia de obtener la lista de peliculas', (done)=>{
    supertest(app).get('/movies/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar get en ruta de movies', ()=>{
  it('deberia de obtener una pelicula con id', (done)=>{
    supertest(app).get('/movies/619d95e02dc7f4cad5f1d856')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar post en ruta de movies', ()=>{
  it('deberia publicar una nueva pelicula', (done)=>{
    supertest(app).post('/movies/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar put en ruta de movies', ()=>{
  it('deberia reemplazar una pelicula', (done)=>{
    supertest(app).put('/movies/619d95e02dc7f4cad5f1d856')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});



describe('Probar delete en ruta de movies', ()=>{
  it('deberia actualizar una pelicula', (done)=>{
    supertest(app).delete('/movies/619d95e02dc7f4cad5f1d856')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});