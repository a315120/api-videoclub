const supertest = require('supertest');
const app = require('../app');

var key="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'a315120@uach.mx', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('Probar get en ruta de los usuarios', ()=>{
  it('deberia de obtener la lista de usuarios', (done)=>{
    supertest(app).get('/users/619d8a88ef0e67ec135e7406')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar get en ruta de los usuarios', ()=>{
  it('deberia de obtener un usuario con id', (done)=>{
    supertest(app).get('/users/id')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar post en ruta de los usuarios', ()=>{
  it('deberia publicar un nuevo usuario', (done)=>{
    supertest(app).post('/users/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar put en ruta de los usuarios', ()=>{
  it('deberia reemplazar un usuario', (done)=>{
    supertest(app).put('/users/619d8a88ef0e67ec135e7406')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar put en ruta de los usuarios', ()=>{
  it('deberia actualizar un usuario', (done)=>{
    supertest(app).patch('/users/619d8a88ef0e67ec135e7406')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});

describe('Probar put en ruta de los usuarios', ()=>{
  it('deberia actualizar un usuario', (done)=>{
    supertest(app).delete('/users/619d8a88ef0e67ec135e7406')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});