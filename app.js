const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const expressJwt = require('express-jwt');
const { accessibleRecordsPlugin } = require('@casl/mongoose');
const config = require('config');
const i18n = require('i18n');
const logger = require('morgan');

const log4js = require("log4js");
let logger4 = log4js.getLogger();
logger4.level="debug"; //production

mongoose.plugin(accessibleRecordsPlugin);


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const actorsRouter = require('./routes/actors');
const moviesRouter = require('./routes/movies');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');
const membersRouter = require('./routes/members');

const jwtKey = config.get("secret.key");
//console.log(jwtKey);

const uri = config.get("dbChain");

mongoose.connect(uri);

const db = mongoose.connection;
const app = express();

db.on('error', ()=>{
  logger4.fatal("No prende >:C");
});

db.on('open', ()=>{
  logger4.info("La app se inicia perron ;)");
});

i18n.configure({
  locales: ['es', 'en'],
  cookie: 'language',
  directory: `${__dirname}/locales`
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

app.use(expressJwt({secret:jwtKey, algorithms:['HS256']})
   .unless({path:["/login"]}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/actors', actorsRouter);
app.use('/movies', moviesRouter);
app.use('/copies', copiesRouter);
app.use('/bookings', bookingsRouter);
app.use('/members', membersRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
